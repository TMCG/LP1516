/* 
Projeto LP 2015/16 2 Semestre IST LEIC-A
Grupo 152
	Nuno Martins 		83535
	Tiago Goncalves 	83567
*/


/*
posicao e' definido como (Linha,Coluna) movimento e' definido como (Dir,Linha_de_chegada,Coluna_de_chegada) Dir pode ser [c,b,e,d]
*/

/*Criacao de Movimentos*/
/*
movs_possiveis/4
	movs_possiveis(Lab,Pos_atual,Movs,Poss)	dado um labirinto(Lab) uma posicao(Pos_atual) e os movimentos anteriores(Movs),
	indica os movimentos possiveis(Poss), sendo que o novo movimento nao pode ir para a uma posicao ja visitada.
*/	

movs_possiveis(Lab,Pos_atual,Movs,Poss):-	see_walls(Lab,Pos_atual,Pos_Walls),
											no_walls([c,b,e,d],Pos_Walls,Path),
											create_movs(Path,Pos_atual,Movs_possiveis),
											remove_bad_movs(Movs_possiveis,Movs,Poss).

/* 
see_walls/3
	see_walls(Lab,Pos,Pos_Walls) dado um labirinto(Lab) uma posicao(Pos) devolve as paredes que estao 
	a volta dessa posicao(Pos_Walls) 
*/
see_walls(Lab,(L,C),Pos_Walls):-	nth1(L,Lab,Linha),
									nth1(C,Linha,Pos_Walls). 

/*
no_walls/3
	no_walls(Dir,Walls,Path) dado as possiveis direcoes(Dir) e as paredes que rodeiam uma posicao(Walls) 
	devolve os caminhos possiveis(Path)
*/
no_walls(L,[],L). 		%Caso de paragem
no_walls(L,[E|R],K):-	select(E,L,L1),
						no_walls(L1,R,K).

/*
create_movs/3
	create_movs(Path,Pos,Movs) dado os caminhos possiveis(Path) numa lista e a posicao(Pos), devolve os movimentos
	possiveis(Movs) na forma definida
*/
create_movs([],_,[]).								%Caso de paragem para lista de caminhos vazia
create_movs(c,(L,C),[(c,L1,C)]):-	L1 is L - 1. 	%Caso de paragem para direcao
create_movs(b,(L,C),[(b,L1,C)]):-	L1 is L + 1. 	%Caso de paragem para direcao
create_movs(e,(L,C),[(e,L,C1)]):-	C1 is C - 1.	%Caso de paragem para direcao
create_movs(d,(L,C),[(d,L,C1)]):-	C1 is C + 1.	%Caso de paragem para direcao
create_movs([Dir|R_Path],Pos,Movs):- 	create_movs(Dir,Pos,Movs1),		
										create_movs(R_Path,Pos,Movs2),
										append(Movs1,Movs2,Movs).

/*
remove_bad_movs/3
	remove_bad_movs(Movs_possiveis,Movs,Good_movs) dado os movimentos possiveis(Movs_possiveis) , os
	movimentos efectuados(Movs) retorna os movimentos que dao origem a uma posicao ainda nao visitada(Good_movs)
*/
remove_bad_movs([],_,[]).	%Caso de paragem
remove_bad_movs([M|R_Movs],Movs,Good_movs):-	verifica_movs(M,Movs,Checked),
												remove_bad_movs(R_Movs,Movs,G1),
												append(Checked,G1,Good_movs).

/*
verifica_movs/3
	verifica_movs(Mov,P_Movs,Res) verifica se o movimento(Mov) ira parar numa posicao ja visitada 
	estando essas posicoes no P_Movs e devolve [] se ja foi visitada ou Movs se ainda nao foi visitada.
*/
verifica_movs((_,L,C),[(_,L,C)|_],[]):- ! .	%Caso de paragem Negativo
/*  ! porque falha e nao pode refazer de modo a que nao falhe senao occore um loop infinito pois poderia voltar a mesma posicao */
verifica_movs(M,[],[M]).			%Caso de paragem Afirmativo
verifica_movs(Mov,[_|R],Res):-	verifica_movs(Mov,R,Res).

/*Fim de criacao de movimentos*/



/* Calculo da distancia */

/*
distancia/3
	distancia(P1,P2,N) calcula a distancia entre dois pontos (P1 , P2) e devolve o valor(N) 
*/
distancia((L1,C1),(L2,C2),N):- N is abs(L1 - L2) + abs(C1 - C2).

/* Fim do calculo da distancia */



/* Ordenacao dos Movimentos */

/*
ordena_poss/4
	Este predicado ordena os movimentos possiveis segundo dois criterios:
		-em primeiro lugar, os movimentos conducentes a menor distancia a uma posicao final
			 -em caso de igualdade, os movimentos conducentes a uma maior distancia a uma posicao inicial.
				-em caso de igualdade novamente devera' segunda a prioridade: c -> b -> e -> d.

	ordena_poss(Poss, Poss_ord, Pos_inicial, Pos_final) 
	significa que Poss_ord e o resultado de ordenar os movimentos possiveis Poss, segundo os criterios acima,
	em relacao a posicao inicial Pos_inicial e a posicao final Pos_final.
*/
ordena_poss([P],[P],_,_). 	% Caso de paragem
ordena_poss([P|R], Poss_ord, Pos_inicial, Pos_final):- 	ordena_poss(R,Poss_ord1,Pos_inicial,Pos_final),
														ordem1(P,Pos_inicial,Pos_final,Poss_ord1,Poss_ord).
/*
ordem1/5
	ordem1(Mov,I,F,Ord_inc,Ordenada) recebe um movimento(Mov) uma posicao inicial(I) e uma posicao final(F)
	e seguindo a condicoes do ordena_poss/4 compara a distancia ao fim de Mov com a dos movimento ja ordanados(Ord_inc) 
	devolvendo a lista(Ordenada) que resulta em juntar o Mov em Ord_inc na posicao correta
*/														
ordem1([],_,_,_,[]). 	% Caso de paragem (acho que este nunca acontece)
ordem1(P,_,_,[],[P]).	% Caso de paragem

% Caso onde Mov esta' mais proximo do fim em relacao ao primeiro elemento da lista Ord_inc
ordem1((Dir1,L1,C1),_,F,[(Dir2,L2,C2)|R],[(Dir1,L1,C1),(Dir2,L2,C2)|R]):- 	distancia((L1,C1),F,N1),
																			distancia((L2,C2),F,N2),
																			N2>N1.
																			
% Caso onde Mov esta'  mais afastado do fim em relacao ao primeiro elemento da lista Ord_inc
ordem1((Dir1,L1,C1),I,F,[(Dir2,L2,C2)|R],[(Dir2,L2,C2)|X]):- 	distancia((L1,C1),F,N1),
																distancia((L2,C2),F,N2),
																N2<N1,
																/*
																	neste caso e necessario compara com o movimentos seguintes
																	ao contrario do primeiro caso
																*/
																ordem1((Dir1,L1,C1),I,F,R,X).

% Caso onde Mov esta' 'a mesma distancia do fim em relacao ao primeiro elemento da lista Ord_inc
ordem1((Dir1,L1,C1),I,F,[(Dir2,L2,C2)|R],Y):- 	distancia((L1,C1),F,N1),
												distancia((L2,C2),F,N2),
												N2 =:= N1,
												% agora necessita verificar distancia ao ponto inicial
												ordem2((Dir1,L1,C1),I,F,[(Dir2,L2,C2)|R],Y).


/*
ordem2/5
	ordem2(Mov,I,F,Ord_inc,Ordenada) recebe um movimento(Mov) uma posicao inicial(I) e uma posicao final(F)
	e seguindo a condicoes do ordena_poss/4 compara a distancia ao inicio de Mov com a dos movimento ja ordanados(Ord_inc) 
	devolvendo a lista(Ordenada) que resulta em juntar o Mov em Ord_inc na posicao correta
*/
% Nao possui Caso de Paragem pois nao e' chamada recursivamente
% Caso onde Mov esta' mais afastado do inicio em relacao ao primeiro elemento da lista Ord_inc
ordem2((Dir1,L1,C1),I,_,[(Dir2,L2,C2)|R],[(Dir1,L1,C1),(Dir2,L2,C2)|R]):-	distancia((L1,C1),I,N1),
																			distancia((L2,C2),I,N2),
																			N2<N1.

% Caso onde Mov esta' mais proximo do inicio em relacao ao primeiro elemento da lista Ord_inc 
ordem2((Dir1,L1,C1),I,F,[(Dir2,L2,C2)|R],[(Dir2,L2,C2)|X]):- 	distancia((L1,C1),I,N1),
																distancia((L2,C2),I,N2),
																N2>N1,
																/*e' necessario verificar se exite mais algum ponto 
																com a mesma distancia ao fim */
																ordem1((Dir1,L1,C1),I,F,R,X).
																

% Caso onde Mov esta' 'a mesma distancia do inicio em relacao ao primeiro elemento da lista Ord_inc
ordem2((Dir1,L1,C1),I,F,[(Dir2,L2,C2)|R],[Or_Dir1|Y]):- 	distancia((L1,C1),I,N1),
															distancia((L2,C2),I,N2),
															N2 =:= N1,
															ordemDir((Dir1,L1,C1),(Dir2,L2,C2),[Or_Dir1,Or_Dir2]),
															/*
																e' necessario verificar se exite mais algum ponto 
																com a mesma distancia ao fim e possivelmente com
																direcao diferente. 
																(isto e' capaz de ser matematicamente impossivel)
															*/
															ordem1(Or_Dir2,I,F,R,Y).
															
/*
ordemDir ordena dois movimentos possuem a mesma distancia ao fim e ao inicio de acordo com a sua direcao
dando a seguinte prioridade na ordenacao c -> b -> e -> d
*/

%Todas as combinacoes possiveis para a comparacao e ordenacao de dois movimentos
ordemDir((c,L1,C1),(b,L2,C2),[(c,L1,C1),(b,L2,C2)]).			
ordemDir((c,L1,C1),(e,L2,C2),[(c,L1,C1),(e,L2,C2)]).
ordemDir((c,L1,C1),(d,L2,C2),[(c,L1,C1),(d,L2,C2)]).
ordemDir((b,L1,C1),(c,L2,C2),[(c,L1,C1),(b,L2,C2)]).
ordemDir((b,L1,C1),(e,L2,C2),[(b,L1,C1),(e,L2,C2)]).
ordemDir((b,L1,C1),(d,L2,C2),[(b,L1,C1),(d,L2,C2)]).
ordemDir((e,L1,C1),(c,L2,C2),[(c,L1,C1),(e,L2,C2)]).
ordemDir((e,L1,C1),(b,L2,C2),[(b,L1,C1),(e,L2,C2)]).
ordemDir((e,L1,C1),(d,L2,C2),[(e,L1,C1),(d,L2,C2)]).
ordemDir((d,L1,C1),(c,L2,C2),[(c,L1,C1),(d,L2,C2)]).
ordemDir((d,L1,C1),(b,L2,C2),[(b,L1,C1),(d,L2,C2)]).
ordemDir((d,L1,C1),(d,L2,C2),[(e,L1,C1),(d,L2,C2)]).

/* Fim da Ordenacao dos Movimentos */


/* Resolucao de labirintos */

/*
resolve1/4
	resolve1(Lab, Pos_inicial,Pos_final, Movs) dado as especificacoes de um labirinto(Lab) a posiciao inicial(Pos_inicial) 
	e a posicao final(Pos_final), devolve os movimentos feitos(Movs) ate chegar a solucao sendo mov uma lista onde o primeiro
	elemento e o inicio, o segundo e o primeiro movimento feito o terceiro o segundo movimento feito e o ultimo elemento e o 
	fim 
*/



resolve1(Lab,(L,C),Pos_final,Sol):- resolve1(Lab,(L,C),Pos_final,Sol,[(i,L,C)]).

resolve1(_,(L,C),(L,C),Movs,Movs).		/* Caso de paragem e quando o ponto atual e igual a' final*/
resolve1(Lab,Pos_atual,Pos_final,Sol,Movs):-	movs_possiveis(Lab,Pos_atual,Movs,Possiveis),
												select((Dir,L,C),Possiveis,_), 
												/*	aqui escolhemos o primeiro membro de possiveis
													mas se falhar ira percorrer todos ate que nao haja membros e falhe*/
												append(Movs,[(Dir,L,C)],N_Movs),
												resolve1(Lab,(L,C),Pos_final,Sol,N_Movs).


/*
resolve2/4
	resolve2(Lab, Pos_inicial,Pos_final, Movs) dado as especificacoes de um labirinto(Lab) a posiciao inicial(Pos_inicial) 
	e a posicao final(Pos_final), devolve os movimentos feitos(Movs) ate chegar a solucao sendo mov uma lista onde o primeiro
	elemento e o inicio, o segundo e o primeiro movimento feito o terceiro o segundo movimento feito e o ultimo elemento e o 
	fim, movimentos sao escolhido de acordo com a distancia ao fim, inicio e direcao.
*/

resolve2(Lab,(L,C),Pos_final,Sol):- resolve2(Lab,(L,C),Pos_final,Sol,[(i,L,C)]).

resolve2(_,(L,C),(L,C),Movs,Movs).		/* Caso de paragem e quando o ponto atual e igual a' final*/
resolve2(Lab,Pos_atual,Pos_final,Sol,[(i,Li,Ci)|R]):-	movs_possiveis(Lab,Pos_atual,[(i,Li,Ci)|R],Possiveis),
														ordena_poss(Possiveis,Possiveis_ord,(Li,Ci),Pos_final),
														select((Dir,L,C),Possiveis_ord,_),
														/*	aqui escolhemos o primeiro membro de possiveis
															mas se falhar ira percorrer todos ate que nao haja membros e falhe*/
														append([(i,Li,Ci)|R],[(Dir,L,C)],N_Movs),
														resolve2(Lab,(L,C),Pos_final,Sol,N_Movs).

/* Fim de Resolucao de Labirintos */